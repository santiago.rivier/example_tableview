#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include "person.h"
#include "foodtablemodel.h"

int main(int argc, char *argv[])
{
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif

    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;

    QObjectList people;
    people.append(new Person("Santiago", "28", "40000"));
    people.append(new Person("Andres", "27", "55000"));
    people.append(new Person("Marina", "30", "25000"));
    engine.rootContext()->setContextProperty("people", QVariant::fromValue(people));

    FoodTableModel *foods = new FoodTableModel;
    engine.rootContext()->setContextProperty("foods", foods);


    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}

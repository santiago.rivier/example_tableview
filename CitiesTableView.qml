import QtQuick 2.9
import QtQuick.Controls 1.4 as C1

C1.TableView {
    model: ListModel {
        ListElement {
            name: "Buenos Aires"
            country: "Argentina"
        }
        ListElement {
            name: "Bogota"
            country: "Colombia"
        }
        ListElement {
            name: "Santiago"
            country: "Chile"
        }
    }
    C1.TableViewColumn {
        role: "name"
        title: "City Name"
        width: 100
    }
    C1.TableViewColumn {
        role: "country"
        title: "Country"
        width: 100
    }
}

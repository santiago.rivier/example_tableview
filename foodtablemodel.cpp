#include "foodtablemodel.h"

FoodTableModel::FoodTableModel(QObject *parent)
    : QAbstractTableModel {parent}
{
    m_foods.append({ "Pizza", "50.0", "A123" });
    m_foods.append({ "Hot Dog", "12.0", "B634" });
    m_foods.append({ "CocaCola", "125.0", "C472" });
}

int FoodTableModel::rowCount(const QModelIndex &parent) const
{
    (void) parent;
    return m_foods.size();
}

int FoodTableModel::columnCount(const QModelIndex &parent ) const
{
    (void) parent;
    return roleNames().size();
}

QVariant FoodTableModel::data(const QModelIndex &index, int role) const
{
    QVariant variant;
    const int row = index.row();
    const int col = role;
    switch (col) {
    case DESCRIPTION:{
        variant = m_foods.at(row).description;
        break;
    }
    case PRICE: {
        variant = m_foods.at(row).price;
        break;
    }
    case CODE: {
        variant = m_foods.at(row).code;
        break;
    }
    }
    return variant;
}

QHash<int,QByteArray> FoodTableModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles.insert(DESCRIPTION, "description");
    roles.insert(PRICE, "price");
    roles.insert(CODE, "code");
    return roles;
}

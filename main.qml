import QtQuick 2.9
import QtQuick.Controls 1.4 as C1
import QtQuick.Controls 2.2

ApplicationWindow {
    width: 640
    height: 480
    visible: true
    title: qsTr("TableView")

    C1.SplitView {
       id: splitview
       anchors.fill: parent
       orientation: Qt.Vertical
       //property int nItems: splitview.children.length

       CitiesTableView {
           height: parent.height/3//splitview.nItems
       }
       PeopleTableView {
           height: parent.height/3//splitview.nItems
       }
       FoodsTableView {
           height: parent.height/3//splitview.nItems
       }
    }
}
